# Whitefox API    
## Installation
- Install [Elixir](http://elixir-lang.org/install.html) & [PostgreSQL](http://www.postgresql.org/download/)
- `git clone https://bitbucket.org/wfxs/site-backend`
- Make sure PostgreSQL connection details match in config (See [config/dev.exs](./config/dev.exs))
- Run `mix deps.get && ecto.setup`
- Run `mix phoenix.server` to start the server

*Note, if you're running Windows you'll need to setup the toolchain for comeonin https://github.com/elixircnx/comeonin/issues/75*