defmodule Whitefox.Repo do
	use Ecto.Repo, otp_app: :whitefox
	use Scrivener, page_size: 15
	import Ecto.Query

	def count(query) do
		query = Ecto.Query.exclude(query, :select)
		one(from c in query, select: fragment("count(*)"))
	end

	def limit(query, val) do
		query = Ecto.Query.exclude(query, :limit)
		from c in query, limit: ^val
	end

	def offset(query, val) do
		query = Ecto.Query.exclude(query, :offset)
		from c in query, offset: ^val
	end

	def execute_and_load(sql, params, model) do
		Ecto.Adapters.SQL.query!(__MODULE__, sql, params)
		|> load_into(model)
	end

	defp load_into(response, model) do
		Enum.map response.rows, fn(row) ->
			fields = Enum.reduce(Enum.zip(response.columns, row), %{},
			fn({key, value}, map) ->
				Map.put(map, key, value)
			end)

			Ecto.Schema.__load__(model, nil, nil, [], fields, &__MODULE__.__adapter__.load/2)
		end
	end
end
