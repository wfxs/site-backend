defmodule Whitefox.User.Registration do
	alias Whitefox.User
	alias Whitefox.Repo
	alias Whitefox.Mailer
	alias Whitefox.Authentication
	alias Ecto.Changeset

	def register_user(changeset) do
		{result, user} =
			changeset
			|> generate_password
			|> generate_verification_code
			|> Repo.insert

		if result == :ok do
			Mailer.send_verification_email(user)
		end

		{result, user}
	end

	def verify_user(verification_code) do
		user = Repo.get_by(User, verification_code: verification_code)
		if user do
			Changeset.change(user, verification_code: nil, permission_level: 0)
			|> Repo.update!
		end
		!!user
	end

	defp generate_verification_code(changeset) do
		Changeset.put_change(
			changeset,
			:verification_code,
			SecureRandom.uuid
		)
	end

	defp generate_password(changeset) do
		encrypted_password =
			if (changeset.params["password"] == nil) do
				nil
			else
				Authentication.hash_password(changeset.params["password"])
			end

		Changeset.put_change(
			changeset,
			:encrypted_password,
			encrypted_password
		)
	end
end
