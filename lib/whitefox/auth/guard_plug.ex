defmodule Whitefox.Authentication.SimpleGuard do
	import Plug.Conn
	use Phoenix.Controller

	alias Whitefox.Authentication

	def init(permission_group), do: permission_group

	def call(conn, permission_group) do
		if Authentication.has(conn, permission_group) do
			conn
		else
			conn
			|> Whitefox.API.error(:no_permission)
			|> halt
		end
	end
end
