defmodule Whitefox.Token do
	import Plug.Conn

	alias Timex.Date
	alias Timex.Time
	alias Whitefox.UserToken
	alias Whitefox.Repo

	def assign(user, lifetime \\ nil) do
		params = %{
			hash:      SecureRandom.uuid,
			refresh:   SecureRandom.uuid,
			user_id:   user.id,
			last_used: nil,
			lifetime:  lifetime
		}

		token = UserToken.changeset(%UserToken{}, params)

		Repo.insert(token)
	end

	def touch(token) do
		token = Ecto.Changeset.change(token,
			last_used: Date.now
		)

		Repo.update!(token)
	end

	def refresh(refresh_hash) do
		token = Repo.get_by(UserToken, refresh: refresh_hash)

		if token do
			token = Ecto.Changeset.change(token,
				hash:    SecureRandom.uuid,
				refresh: SecureRandom.uuid)

			{:ok, Repo.update!(token)}
		else
			{:error, "Failed to fetch token"}
		end
	end

	def retrieve(conn) do
		extracted_token = extract_hash(conn)

		if extracted_token do
			token = UserToken
				|> UserToken.with_user
				|> Repo.get(extracted_token)

			if token do
				if valid?(token) do
					touch(token)
					{:ok, token}
				else
					{:error, "Token has expired"}
				end
			else
				{:error, "Failed to fetch token"}
			end
		else
			false
		end
	end

	def valid?(token) do
		!is_nil(token) &&
		(
			is_nil(token.lifetime) ||
			Date.compare(Date.now, death_date(token)) == -1
		)
	end

	def extract_hash(conn) do
		case get_req_header(conn, "authorization") do
			["Token " <> token_hash] -> token_hash
			_                        -> false
		end
	end

	def death_date(token) do
		if is_nil(token.lifetime) do
			nil
		else
			Date.add(
				token.updated_at,
				Time.to_timestamp(token.lifetime, :secs)
			)
		end
	end
end
