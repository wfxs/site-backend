defmodule Whitefox.Token.Plug do
	import Plug.Conn
	alias Whitefox.Token

	def init(opts), do: opts

	def call(conn, _opts) do
		case Token.retrieve(conn) do
			{:ok,    token} -> assign(conn, :token, {:valid,   token})
			{:error, error} -> assign(conn, :token, {:invalid, error})
			false           -> conn
		end
	end
end
