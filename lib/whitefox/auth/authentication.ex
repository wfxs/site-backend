defmodule Whitefox.Authentication do

	alias Whitefox.User
	import Comeonin.Pbkdf2

	def login(login, password_unsafe) do
		user = User.get_by_login(login)

		if user do
			if checkpw(password_unsafe, user.encrypted_password) do
				{:ok, user}
			else
				{:error, "Login does not exist"}
			end
		else
			dummy_checkpw()
			{:error, "Login does not exist"}
		end
	end

	def user(conn) do
		case conn.assigns[:token] do
			{:valid, token} -> token.user
			_               -> nil
		end
	end

	def is_user_id(conn, user_id) do
		user = user(conn)
		if user do
			user.id == user_id
		else
			false
		end
	end

	def has(conn, permission_group) do
		user_permission =
			case conn.assigns[:token] do
				nil                -> -1
				{:invalid, _     } -> -2
				{:valid,   token } -> token.user.permission_level
			end

		user_permission >= permission_group_to_level(permission_group)
	end

	def hash_password(password_unsafe) do
		hashpwsalt(password_unsafe)
	end

	def permission_level_to_group(permission_level) do
		case permission_level do
			  3 -> :admin
			  2 -> :moderator
				1 -> :member
			  0 -> :registered
				_ -> :guest
		end
	end

	defp permission_group_to_level(permission_group) do
		case permission_group do
			:admin      -> 3
			:moderator  -> 2
			:member     -> 1
			:registered -> 0
			_           -> -1
		end
	end
end
