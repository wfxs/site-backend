defmodule Whitefox.Mailer do
	use Mailgun.Client,
		domain: Application.get_env(:whitefox, :mailgun_domain),
		key: Application.get_env(:whitefox, :mailgun_key)

	@from "Whitefox Support <noreply@wfx.es>"

	def send_verification_email(user) do
		send_email to: user.email,
		from: @from,
		subject: "Verify your Whitefox account",
		html: Phoenix.View.render_to_string(Whitefox.EmailView, "verify.html", user: user)
	end
end
