defimpl Poison.Encoder, for: Timex.DateTime do
	use Timex

	def encode(d, _options) do
		fmt = DateFormat.format!(d, "{ISO}")
		"\"#{fmt}\""
	end
end
