defmodule Whitefox.Endpoint do
	use Phoenix.Endpoint, otp_app: :whitefox

	plug Plug.Static,
		at: "/", from: :whitefox, gzip: false,
		only: ~w(favicon.ico robots.txt)

	if code_reloading? do
		plug Phoenix.CodeReloader
	end

	plug Plug.RequestId
	plug Plug.Logger

	plug Whitefox.Token.Plug

	plug Plug.Parsers,
		parsers: [:urlencoded, :multipart, :json],
		pass: ["*/*"],
		json_decoder: Poison

	plug Plug.MethodOverride
	plug Plug.Head

	plug CORSPlug, [origin: "*"]

	plug Whitefox.Router
end
