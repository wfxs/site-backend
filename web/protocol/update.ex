defmodule Whitefox.Protocols.Update do
	use Whitefox.Web, :controller

	def success(conn, model) do
		render(conn, "show.json", model)
	end

	def error(conn, changeset) do
		conn
		|> put_status(:unprocessable_entity)
		|> render(Whitefox.ChangesetView, "error.json", changeset: changeset)
	end
end
