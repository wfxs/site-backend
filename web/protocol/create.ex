defmodule Whitefox.Protocols.Create do
	use Whitefox.Web, :controller

	def success(conn, route, params) do
		conn
		|> put_status(:created)
		|> put_resp_header("location", route)
		|> render("show.json", params)
	end

	def error(conn, changeset) do
		conn
		|> put_status(:unprocessable_entity)
		|> render(Whitefox.ChangesetView, "error.json", changeset: changeset)
	end
end
