defmodule Whitefox.Router do
	use Whitefox.Web, :router

	pipeline :api do
		plug :accepts, ["json"]
	end

	scope "/", Whitefox do
		pipe_through :api

		resources "/tokens", TokenController,          only: [:create, :delete], param: "hash"
	end

	scope "/users", Whitefox do
		pipe_through :api

		get "/handle/:handle",            UserController, :redirect_handle
		get "/verify/:verfication_code",  UserController, :verify

		resources "/",  UserController,                except: [:new, :edit]
	end

	scope "/", Whitefox.Articles do
		pipe_through :api

		resources "/articles", ArticleController,      except: [:new, :edit]
	end

	scope "/forum", Whitefox.Forum do
		pipe_through :api

		resources "/posts",       PostController,      except: [:new, :edit]
		resources "/threads",     ThreadController,    except: [:new, :edit]
		resources "/categories",  CategoryController,  except: [:new, :edit]
		resources "/directories", DirectoryController, only:   [:show]
	end
end
