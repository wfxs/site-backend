defmodule Whitefox.User do
	use Whitefox.Web, :model

	alias Whitefox.Repo
	alias Whitefox.User

	schema "users" do
		field :handle,                :string
		field :name,                  :string
		field :date_of_birth,         Ecto.DateTime
		field :location,              :string
		field :encrypted_password,    :string, allow_nil: false
		field :email,                 :string
		field :password,              :string, virtual: true

		field :permission_level,      :integer
		field :verification_code,     :string

		timestamps
	end

	@required_fields ~w(handle password email)
	@optional_fields ~w(name date_of_birth location)

	def get_by_login(login) do
		query = from u in User,
						select: u,
						where: u.handle == ^login or u.email == ^login

		Repo.one(query)
	end

	def changeset(model, params \\ nil) do
		model
		|> cast(params, @required_fields, @optional_fields)
		|> validate_length(:password, min: 5)
		|> unique_constraint(:handle, on: Whitefox.Repo)
		|> unique_constraint(:email, on: Whitefox.Repo)
	end
end
