defmodule Whitefox.UserToken do
	use Whitefox.Web, :model

	@primary_key {:hash, :string, []}
	schema "user_tokens" do
		field      :refresh,   :string
		field      :lifetime,  :integer # In seconds
		field      :last_used, Timex.Ecto.DateTime
		belongs_to :user,      Whitefox.User, references: :id

		timestamps
	end

	@required_fields ~w(hash refresh user_id)
	@optional_fields ~w(lifetime last_used)

	def with_user(query) do
		from q in query, preload: :user
	end

	def changeset(model, params \\ nil) do
		model
		|> cast(params, @required_fields, @optional_fields)
		|> assoc_constraint(:user)
	end
end
