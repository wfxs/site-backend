defmodule Whitefox.Articles.Article do
	use Whitefox.Web, :model

	schema "articles" do
		field      :title,    :string
		field      :body,     :string

		belongs_to :author,   Whitefox.User

		timestamps
	end

	@required_fields ~w(title author_id body)
	@optional_fields ~w()

	def with_author(query) do
		from p in query,
			preload: :author
	end

	def changeset(model, params \\ :empty) do
		model
		|> cast(params, @required_fields, @optional_fields)
		|> assoc_constraint(:author)
	end
end
