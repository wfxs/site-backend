defmodule Whitefox.Forum.Category do
	use Whitefox.Web, :model

	alias Whitefox.Repo
	alias Whitefox.Forum.Thread
	alias Whitefox.Forum.Category

	schema "categories" do
		field :title,       :string
		field :description, :string
		field :parent_id,   :integer

		belongs_to :author, Whitefox.User

		timestamps
	end

	@required_fields ~w(title description author_id)
	@optional_fields ~w(parent_id)

	def get_subcategories(category_id) do
		from c in Category,
		select: c,
		where: c.parent_id == ^category_id
	end

	def get_threads(category_id) do
		from t in Thread,
		select: t,
		where: t.category_id == ^category_id
	end

	def get_category_tree(category_id) do
		query = """
		WITH RECURSIVE category_tree AS
		(
			SELECT id, title, description, parent_id
			FROM categories
			WHERE id = $1
			UNION ALL
			SELECT c.id, c.title, c.description, c.parent_id
			FROM categories AS c
			INNER JOIN category_tree AS ct
			ON (c.id = ct.parent_id)
		)
		SELECT id, title, description
		FROM category_tree
		"""

		result = Repo.execute_and_load(query, [category_id], Category) || []

		Enum.reverse(result)
	end

	def changeset(model, params \\ :empty) do
		model
		|> cast(params, @required_fields, @optional_fields)
		|> assoc_constraint(:author)
	end
end
