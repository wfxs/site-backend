defmodule Whitefox.Forum.Post do
	use Whitefox.Web, :model

	schema "posts" do
		field      :body,   :string

		belongs_to :author, Whitefox.User
		belongs_to :thread, Whitefox.Forum.Thread

		timestamps
	end

	@required_fields ~w(body author_id thread_id)
	@optional_fields ~w()

	def with_author(query) do
		from q in query, preload: :author
	end

	def from_thread(query, thread_id) do
		from q in query, where: q.thread_id == ^thread_id
	end

	def changeset(model, params \\ :empty) do
		model
		|> cast(params, @required_fields, @optional_fields)
		|> assoc_constraint(:author)
		|> assoc_constraint(:thread)
	end
end
