defmodule Whitefox.Forum.Thread do
	use Whitefox.Web, :model

	schema "threads" do
		field      :title,    :string

		belongs_to :author,   Whitefox.User
		belongs_to :category, Whitefox.Forum.Category

		timestamps
	end

	@required_fields ~w(title author_id category_id)
	@optional_fields ~w()

	def with_author(query) do
		from p in query,
			preload: :author
	end

	def changeset(model, params \\ :empty) do
		model
		|> cast(params, @required_fields, @optional_fields)
		|> assoc_constraint(:author)
		|> assoc_constraint(:category)
	end
end
