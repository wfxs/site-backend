defmodule Whitefox.Articles.ArticleController do
	use Whitefox.Web, :controller

	alias Whitefox.Articles.Article

	plug Authentication.SimpleGuard, :admin when action in [:delete, :create, :update]

	def index(conn, params) do
		articles = Article
		|> Article.with_author
		|> Repo.paginate(params)

		render(conn, "index.json", articles: articles)
	end

	def show(conn, %{ "id" => id }) do
		article =
			Article
			|> Article.with_author
			|> Repo.get!(id)

		render(conn, "show.json", article: article)
	end

	def create(conn, article_params) do
		changeset = Article.changeset(%Article{}, article_params)

		if Authentication.is_user_id(conn, changeset.changes[:author_id]) do
			case Repo.insert(changeset) do
				{:ok, article} ->
					article = Repo.preload(article, [:author])
					Protocols.Create.success(
						conn, article_path(conn, :show, article), article: article
					)
				{:error, changeset} ->
					Protocols.Create.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def update(conn, article_params) do
		article = Article
		|> Article.with_author
		|> Repo.get!(article_params["id"])

		if Authentication.is_user_id(conn, article.author_id) do
			changeset = Article.changeset(article, article_params)

			case Repo.update(changeset) do
				{:ok, article} ->
					Protocols.Update.success(
						conn, article: article
					)
				{:error, changeset} ->
					Protocols.Update.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def delete(conn, %{ "id" => id }) do
		article = Repo.get!(Article, id)
		Repo.delete!(article)
		send_resp(conn, :no_content, "")
	end
end
