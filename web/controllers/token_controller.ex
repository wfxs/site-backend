defmodule Whitefox.TokenController do
	use Whitefox.Web, :controller

	alias Whitefox.Token
	alias Whitefox.UserToken

	def create(conn, request_params) do
		case request_params do
			%{ "type" => "refresh", "refresh_token" => refresh_token } ->
				create_token(conn, refresh_token: refresh_token)
			%{ "type" => "credentials", "login" => login, "password" => password_unsafe } ->
				create_token(conn, login: login, password_unsafe: password_unsafe)
				_ -> API.error(conn, :bad_authorisation)
		end
	end

	def delete(conn, %{ "hash" => hash }) do
		token = Repo.get!(UserToken, hash)
		Repo.delete!(token)
		send_resp(conn, :no_content, "")
	end

	defp create_token(conn, refresh_token: refresh_token) do
		case Token.refresh(refresh_token) do
			{:ok, token} ->
				conn
				|> put_status(:created)
				|> render("show.json", token: token)
			{:error, _} -> API.error(conn, :bad_authorisation)
		end
	end

	defp create_token(conn, login: login, password_unsafe: password_unsafe) do
		case Authentication.login(login, password_unsafe) do
			{:ok, user} ->
				case Token.assign(user) do
					{:ok, token} ->
						conn
						|> put_status(:created)
						|> render("show.json", token: token)

					{:error, changeset} ->
						Protocols.Create.error(
							conn, changeset
						)
				end
			{:error, _} -> API.error(conn, :bad_authorisation)
		end
	end
end
