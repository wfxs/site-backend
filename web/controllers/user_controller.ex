defmodule Whitefox.UserController do
	use Whitefox.Web, :controller

	alias Whitefox.User
	alias Whitefox.User.Registration

	plug Authentication.SimpleGuard, :admin when action in [:delete]

	def index(conn, params) do
		users = Repo.paginate(User, params)
		render(conn, "index.json", users: users)
	end

	def show(conn, %{ "id" => id }) do
		user = Repo.get!(User, id)
		render(conn, "show.json", user: user)
	end

	def create(conn, user_params) do
		changeset = User.changeset(%User{}, user_params)

		if changeset.valid? do
			case Registration.register_user(changeset) do
				{:ok, user} ->
					Protocols.Create.success(
						conn, user_path(conn, :show, user), user: user
					)
				{:error, changeset} ->
					Protocols.Create.error(
						conn, changeset
					)
			end
		else
			Protocols.Create.error(
				conn, changeset
			)
		end
	end

	def update(conn, user_params) do
		user = Repo.get!(User, user_params["id"])

		if Authentication.is_user_id(conn, user.id)
		   || Authentication.has(conn, :admin) do

			changeset = User.changeset(user, user_params)

			case Repo.update(changeset) do
				{:ok, user} ->
					Protocols.Update.success(
						conn, user: user
					)
				{:error, changeset} ->
					Protocols.Update.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def delete(conn, %{ "id" => id }) do
		user = Repo.get!(User, id)
		Repo.delete!(user)
		send_resp(conn, :no_content, "")
	end

	def redirect_handle(conn, %{ "handle" => handle }) do
		user = Repo.get_by!(User, handle: handle)
		redirect(conn, to: user_path(conn, :show, user))
	end

	def verify(conn, %{ "verfication_code" => verfication_code }) do
		if Registration.verify_user(verfication_code) do
			# Temp til better method
			redirect(conn, external: "http://beta.wfx.es")
		else
			API.error(conn, :not_found)
		end
	end
end
