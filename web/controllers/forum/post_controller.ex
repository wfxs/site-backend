defmodule Whitefox.Forum.PostController do
	use Whitefox.Web, :controller

	alias Whitefox.Forum.Post

	plug Authentication.SimpleGuard, :moderator  when action in [:delete, :update]
	plug Authentication.SimpleGuard, :registered when action in [:create]

	def index(conn, params) do
		thread_id =
			case params["thread_id"] do
				nil -> 0
				_	 -> String.to_integer(params["thread_id"])
			end

		post_query = Post.with_author(Post)

		if thread_id > 0 do
			post_query = Post.from_thread(post_query, thread_id)
		end

		posts = Repo.paginate(post_query, params)

		render(conn, "index.json", posts: posts)
	end

	def show(conn, %{ "id" => id }) do
		post =
			Post
			|> Post.with_author
			|> Repo.get!(id)

		render(conn, "show.json", post: post)
	end

	def create(conn, post_params) do
		changeset = Post.changeset(%Post{}, post_params)

		if Authentication.is_user_id(conn, changeset.changes[:author_id]) do
			case Repo.insert(changeset) do
				{:ok, post} ->
					post = Repo.preload(post, [:author])
					Protocols.Create.success(
						conn, post_path(conn, :show, post), post: post
					)
				{:error, changeset} ->
					Protocols.Create.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def update(conn, post_params) do
		post =
			Post
			|> Post.with_author
			|> Repo.get!(post_params["id"])

		if Authentication.is_user_id(conn, post.author_id)
		   || Authentication.has(conn, :moderator) do

			changeset = Post.changeset(post, post_params)

			case Repo.update(changeset) do
				{:ok, post} ->
					Protocols.Update.success(
						conn, post: post
					)
				{:error, changeset} ->
					Protocols.Update.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def delete(conn, %{"id" => id}) do
		post = Repo.get!(Post, id)

		if Authentication.is_user_id(conn, post.author_id)
		   || Authentication.has(conn, :moderator) do
			Repo.delete!(post)
			send_resp(conn, :no_content, "")
		else
			API.error(conn, :no_permission)
		end
	end
end
