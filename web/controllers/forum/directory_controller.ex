defmodule Whitefox.Forum.DirectoryController do
	use Whitefox.Web, :controller

	alias Whitefox.Forum.Thread
	alias Whitefox.Forum.Category

	def show(conn, params) do
		#{ api_opts, params } = Whitefox.API.Params.parse(params)

		category_id = String.to_integer(params["id"])

		page_size =
			case params["page_size"] do
				nil -> 15
				_   -> String.to_integer(params["page_size"])
			end

		page_number =
			case params["page"] do
				nil -> 1
				_   -> String.to_integer(params["page"])
			end

		if page_size < 1 || page_size > 50 do
			conn
			|> put_status(:unprocessable_entity)
			|> render("error.json", error: %{
					id: "out_of_range",
					message: "page_size out of accepted range (1 - 50)"
				}
			)
		end

		categories_query = Category.get_subcategories(category_id)
		threads_query    = Category.get_threads(category_id)

		total_categories = Repo.count(categories_query)
		total_threads    = Repo.count(threads_query)
		total_pages      = round(Float.ceil((total_categories + total_threads) / page_size))

		categories_offset = page_size * ( page_number - 1 )
		thread_offset     = categories_offset - total_categories

		categories =
			if categories_offset < total_categories do
				categories_query
				|> Repo.limit(page_size)
				|> Repo.offset(categories_offset)
				|> Repo.all
			else
				[]
			end

		threads =
			if (total_threads > 0 && (thread_offset > 0 || (length(categories) < page_size))) do
				if thread_offset < 0 do
					thread_offset = 0
				end

				threads_query
				|> Thread.with_author
				|> Repo.limit(page_size - length(categories))
				|> Repo.offset(thread_offset)
				|> Repo.all
			else
				[]
			end

		if length(threads) == 0 && length(categories) == 0 do
			render conn, Whitefox.ErrorView, "404.json"
		else
			total_entries = total_threads + total_categories

			parents = Category.get_category_tree(category_id) || []

			render conn, "show.json",
				threads: threads,
				categories: categories,
				breadcrumbs: parents,
				pagination: %{
					page_size: page_size,
					page_number: page_number,
					total_pages: total_pages,
					total_entries: total_entries
				},
				permissions: %{}
			end
	end
end
