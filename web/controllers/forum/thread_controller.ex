defmodule Whitefox.Forum.ThreadController do
	use Whitefox.Web, :controller

	alias Whitefox.Forum.Thread
	alias Whitefox.Forum.Category

	plug Authentication.SimpleGuard, :moderator when action in [:delete]
	plug Authentication.SimpleGuard, :member    when action in [:create, :update]

	def index(conn, params) do
		threads = Thread
		|> Thread.with_author
		|> Repo.paginate(params)

		render(conn, "index.json", threads: threads)
	end

	def show(conn, %{ "id" => id }) do
		thread =
			Thread
			|> Thread.with_author
			|> Repo.get!(id)

		breadcrumbs = Category.get_category_tree(thread.category_id) || []
		render(conn, "show.json", thread: thread, breadcrumbs: breadcrumbs)
	end

	def create(conn, thread_params) do
		changeset = Thread.changeset(%Thread{}, thread_params)

		if Authentication.is_user_id(conn, changeset.changes[:author_id]) do
			case Repo.insert(changeset) do
				{:ok, thread} ->
					thread = Repo.preload(thread, [:author])
					Protocols.Create.success(
						conn, thread_path(conn, :show, thread), thread: thread
					)
				{:error, changeset} ->
					Protocols.Create.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def update(conn, thread_params) do
		thread = Thread
		|> Thread.with_author
		|> Repo.get!(thread_params["id"])

		if Authentication.is_user_id(conn, thread.author_id)
		   || Authentication.has(conn, :moderator) do
			changeset = Thread.changeset(thread, thread_params)

			case Repo.update(changeset) do
				{:ok, thread} ->
					Protocols.Update.success(
						conn, thread: thread
					)
				{:error, changeset} ->
					Protocols.Update.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :no_permission)
		end
	end

	def delete(conn, %{ "id" => id }) do
		thread = Repo.get!(Thread, id)
		Repo.delete!(thread)
		send_resp(conn, :no_content, "")
	end
end
