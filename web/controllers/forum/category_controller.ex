defmodule Whitefox.Forum.CategoryController do
	use Whitefox.Web, :controller

	alias Whitefox.Forum.Category

	plug Authentication.SimpleGuard, :admin when action in [:create, :update, :delete]

	def index(conn, params) do
		categories = Repo.paginate(Category, params)
		render(conn, "index.json", categories: categories)
	end

	def show(conn, %{ "id" => id }) do
		category = Repo.get!(Category, id)
		render(conn, "show.json", category: category)
	end

	def create(conn, category_params) do
		changeset = Category.changeset(%Category{}, category_params)

		parent_id = category_params["parent_id"]

		if parent_id && Repo.get(Category, parent_id) do
			case Repo.insert(changeset) do
				{:ok, category} ->
					Protocols.Create.success(
						conn, category_path(conn, :show, category), category: category
					)
				{:error, changeset} ->
					Protocols.Create.error(
						conn, changeset
					)
			end
		else
			API.error(conn, :unprocessable, "Parent category specified does not exist")
		end
	end

	def update(conn, category_params) do
		category = Repo.get!(Category, category_params["id"])
		changeset = Category.changeset(category, category_params)

		case Repo.update(changeset) do
			{:ok, category} ->
				Protocols.Update.success(
					conn, category: category
				)
			{:error, changeset} ->
				Protocols.Update.error(
					conn, changeset
				)
		end
	end

	def delete(conn, %{ "id" => id }) do
		category = Repo.get!(Category, id)
		Repo.delete!(category)
		send_resp(conn, :no_content, "")
	end
end
