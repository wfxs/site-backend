defmodule Whitefox.API do
	use Phoenix.Controller

	def success(conn, :ok) do
		conn
		|> send_resp(200, "")
	end

	def error(conn, %{ type: type, message: message }) do
		render(conn, Whitefox.ErrorView, "error.json", type: type, message: message)
	end

	def error(conn, :not_found) do
		conn
		|> put_status(404)
		|> error(%{type: "not_found", message: "Not found"})
	end

	def error(conn, :bad_authorisation) do
		conn
		|> put_status(401)
		|> error(%{type: "authorisation", message: "Authorisation failed"})
	end

	def error(conn, :no_permission) do
		conn
		|> put_status(:forbidden)
		|> error(%{type: "permission", message: "You don't have the required permissions"})
	end

	def error(conn, :unprocessable, reason) do
		conn
		|> put_status(:unprocessable_entity)
		|> error(%{type: "unprocessable", message: reason})
	end
end
