defmodule Whitefox.UserView do
	use Whitefox.Web, :view

	alias Whitefox.Authentication

	def render("index.json", %{ users: users }) do
		%{
			_pagination: generate_pagination(users),
			users: render_many(
				users.entries,
				Whitefox.UserView,
				"user.json"
			)
		}
	end

	def render("show.json", %{ user: user }) do
		render_one(user, Whitefox.UserView, "user.json")
	end

	def render("user.json", %{ user: user }) do
		%{
			id: user.id,
			handle: user.handle,
			joined: user.inserted_at,
			power: Authentication.permission_level_to_group(user.permission_level)
		}
	end
end
