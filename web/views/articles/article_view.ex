defmodule Whitefox.Articles.ArticleView do
	use Whitefox.Web, :view

	def render("index.json", %{ articles: articles }) do
		%{
			_pagination: generate_pagination(articles),
			articles: render_many(
				articles.entries,
				Whitefox.Articles.ArticleView,
				"article.json"
			)
		}
	end

	def render("show.json", %{ article: article }) do
		render_one(
			article,
			Whitefox.Articles.ArticleView,
			"article.json"
		)
	end

	def render("article.json", %{ article: article }) do
		%{
			id: article.id,
			title: article.title,
			body: article.body,
			author_handle: article.author.handle,
			created: article.inserted_at,
			updated: article.updated_at
		}
	end
end
