defmodule Whitefox.Forum.CategoryView do
	use Whitefox.Web, :view

	def render("index.json", %{ categories: categories }) do
		%{
			_pagination: generate_pagination(categories),
			categories: render_many(
				categories.entries,
				Whitefox.Forum.CategoryView,
				"category.json"
			)
		}
	end

	def render("show.json", %{
		category: category,
		breadcrumbs: breadcrumbs }) do

		data = render_one(
			category,
			Whitefox.Forum.CategoryView,
			"category.json"
		)

		Map.put(data, :_meta, %{
			breadcrumbs: render_many(
					breadcrumbs,
					Whitefox.Forum.BreadcrumbView,
					"breadcrumb.json",
					as: :breadcrumb
				)
			}
		)
	end

	def render("show.json", %{ category: category }) do
		render_one(
			category,
			Whitefox.Forum.CategoryView,
			"category.json"
		)
	end

	def render("category.json", %{ category: category }) do
		 %{
			id: category.id,
			title: category.title,
			description: category.description,
			parent_id: category.parent_id
		}
	end
end
