defmodule Whitefox.Forum.BreadcrumbView do
	def render("breadcrumb.json", %{ breadcrumb: breadcrumb }) do
		%{
			id: breadcrumb.id,
			title: breadcrumb.title
		}
	end
end
