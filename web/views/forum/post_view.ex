defmodule Whitefox.Forum.PostView do
	use Whitefox.Web, :view

	def render("index.json", %{ posts: posts }) do
		%{
			_pagination: generate_pagination(posts),
			posts: render_many(
				posts.entries,
				Whitefox.Forum.PostView,
				"post.json"
			)
		}
	end

	def render("show.json", %{ post: post }) do
		render_one(post, Whitefox.Forum.PostView, "post.json")
	end

	def render("post.json", %{ post: post }) do
		%{
			id: post.id,
			body: post.body,
			created: post.inserted_at,
			updated: post.updated_at,
			author_handle: post.author.handle,
			thread_id: post.thread_id
		}
	end
end
