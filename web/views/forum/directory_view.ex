defmodule Whitefox.Forum.DirectoryView do
	use Whitefox.Web, :view

	def render("index.json", %{
			threads: threads,
			categories: categories,
			breadcrumbs: breadcrumbs,
			pagination: pagination }) do
		%{
			threads: render_many(
				threads,
				Whitefox.Forum.DirectoryView,
				"thread.json",
				as: :thread
			),
			categories: render_many(
				categories,
				Whitefox.Forum.DirectoryView,
				"category.json",
				as: :category
			),
			_meta: %{
				breadcrumbs: render_many(
					breadcrumbs,
					Whitefox.Forum.BreadcrumbView,
					"breadcrumb.json",
					as: :breadcrumb
				)
			},
			_pagination: pagination
		}
	end

	def render("show.json", %{
		threads: threads,
		categories: categories,
		breadcrumbs: breadcrumbs,
		pagination: pagination,
		permissions: permissions }) do
		%{
			threads: render_many(
				threads,
				Whitefox.Forum.DirectoryView,
				"thread.json",
				as: :thread
			),
			categories: render_many(
				categories,
				Whitefox.Forum.DirectoryView,
				"category.json",
				as: :category
			),
			_meta: %{
				breadcrumbs: render_many(
					breadcrumbs,
					Whitefox.Forum.BreadcrumbView,
					"breadcrumb.json",
					as: :breadcrumb
				)
			},
			_permissions: permissions,
			_pagination: pagination,
		}
	end

	def render("thread.json", %{ thread: thread }) do
		%{
			id: thread.id,
			title: thread.title
			#description: thread.description
		}
	end

	def render("category.json", %{ category: category }) do
		%{
			id: category.id,
			title: category.title,
			description: category.description
		}
	end
end
