defmodule Whitefox.Forum.ThreadView do
	use Whitefox.Web, :view

	def render("index.json", %{ threads: threads }) do
		%{
			_pagination: generate_pagination(threads),
			threads: render_many(
				threads.entries,
				Whitefox.Forum.ThreadView,
				"thread.json"
			)
		}
	end

	def render("show.json", %{
		thread: thread,
		breadcrumbs: breadcrumbs }) do

		data = render_one(
			thread,
			Whitefox.Forum.ThreadView,
			"thread.json"
		)

		Map.put(data, :_meta, %{
			breadcrumbs: render_many(
					breadcrumbs,
					Whitefox.Forum.BreadcrumbView,
					"breadcrumb.json",
					as: :breadcrumb
				)
			}
		)
	end

	def render("show.json", %{ thread: thread }) do
		render_one(
			thread,
			Whitefox.Forum.ThreadView,
			"thread.json"
		)
	end

	def render("thread.json", %{ thread: thread }) do
		%{
			id: thread.id,
			title: thread.title,
			category_id: thread.category_id,
			author_handle: thread.author.handle
		}
	end
end
