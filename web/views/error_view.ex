defmodule Whitefox.ErrorView do
	use Whitefox.Web, :view

	def render("error.json", params) do
		result = %{
			type:   params.type,
			message: params.message
		}

		if Map.has_key?(params, :errors) do
			result = Map.put(result, :errors, params.errors)
		end

		%{
			error: result
		}
	end

	def render("404.json", _assigns) do
		render "error.json", type: "not_found", message: "Not found"
	end

	def render("500.json", _assigns) do
		render "error.json", type: "server_error", message: "Server internal error"
	end

	def template_not_found(_template, assigns) do
		render "500.json", assigns
	end
end
