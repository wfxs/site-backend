defmodule Whitefox.ViewUtils do

	def generate_pagination(%{
		page_size: page_size,
		page_number: page_number,
		total_pages: total_pages,
		total_entries: total_entries }) do
		%{
			page_size: page_size,
			page_number: page_number,
			total_pages: total_pages,
			total_entries: total_entries
		}
	end
end
