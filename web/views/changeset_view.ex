defmodule Whitefox.ChangesetView do
	use Whitefox.Web, :view

	alias Ecto.Changeset

	def render("error.json", %{ changeset: changeset }) do
		render Whitefox.ErrorView, "error.json",
			type: "validation",
			message: "Bad data",
			errors: build_errors(changeset)
	end

	def build_errors(%Changeset{ errors: errors }) do
		Enum.map errors, fn { field, error } ->
			%{
				path: field,
				message: render_message(error)
			}
		end
	end

	def render_message(message) when is_binary(message) do
		message
	end

	def render_message({ message, values }) do
		Enum.reduce values, message, fn {k, v}, acc ->
			String.replace(acc, "%{#{k}}", to_string(v))
		end
	end
end
