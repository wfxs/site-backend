defmodule Whitefox.TokenView do
	use Whitefox.Web, :view

	alias Whitefox.Token

	def render("show.json", %{ token: token }) do
		%{
			hash:    token.hash,
			refresh: token.refresh,
			expires: Token.death_date(token),
			user_id: token.user_id
		}
	end
end
