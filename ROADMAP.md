# API
### USERS
- ~~ISCUD~~
- ~~Permissions~~

##### Advanced
- Change avatar URL

### Token System
- ~~Generation~~
- ~~Verification~~
- ~~Extraction~~
- ~~Refresh Tokens~~
- ~~Deletion~~
- CRON clean

### FORUMS
- ~~Categories, Threads, Posts (ISCUD)~~
- ~~Permissions~~

##### Advanced
- Delete post
- Thank post
- Flag post

### ARTICLES
- ~~ISCUD~~

### SERVER LIST
- View list

##### Advanced
- Current players online
- Player history

### Misc
- Need to standardise error types returned 
- Need to work on model validation
- Write better tests