use Mix.Config

config :whitefox, Whitefox.Endpoint,
	http: [port: {:system, "PORT"}],
	url: [host: System.get_env("HOSTNAME")],
	secret_key_base: System.get_env("SECRET_KEY_BASE")

config :whitefox, Whitefox.Repo,
	adapter: Ecto.Adapters.Postgres,
	url: System.get_env("DATABASE_URL"),
	size: 20

# Do not print debug messages in production
config :logger, level: :info

config :whitefox,
	mailgun_domain: "https://api.mailgun.net/v3/mg.wfx.es",
	mailgun_key: System.get_env("MAILGUN_API_KEY")
