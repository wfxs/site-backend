use Mix.Config

# Configures the endpoint
config :whitefox, Whitefox.Endpoint,
	url: [host: "localhost"],
	root: Path.dirname(__DIR__),
	secret_key_base: "QjTItOUTn2sgOBvrgUFSP1XKIeA2eWWv6SplFLMMwXzcozBXAsLKQEWecPr915ry",
	render_errors: [accepts: ~w(json)],
	pubsub: [name: Whitefox.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
	format: "$time $metadata[$level] $message\n",
	metadata: [:request_id]

# Import environment specific config.
import_config "#{Mix.env}.exs"

# Configure phoenix generators
config :phoenix, :generators,
	migration: true,
	binary_id: false
