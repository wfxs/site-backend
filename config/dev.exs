use Mix.Config

config :whitefox, Whitefox.Endpoint,
	http: [port: 4000],
	code_reloader: true,
	check_origin: false
	#debug_errors: true

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development.
# Do not configure such in production as keeping
# and calculating stacktraces is usually expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :whitefox, Whitefox.Repo,
	adapter: Ecto.Adapters.Postgres,
	username: "postgres",
	password: "postgres",
	database: "whitefox_dev",
	hostname: "localhost",
	pool_size: 10

config :whitefox,
	mailgun_domain: "https://api.mailgun.net/v3/sandbox991851e8ae1b49429823332ac9fefa86.mailgun.org",
	mailgun_key: "key-36ba4b1fef805ebb256f3783c235ac8f"
