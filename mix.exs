defmodule Whitefox.Mixfile do
	use Mix.Project

	def project do
		[
			app: :whitefox,
			version: "0.0.3",
			elixir: "~> 1.0",
			elixirc_paths: elixirc_paths(Mix.env),
			compilers: [:phoenix] ++ Mix.compilers,
			build_embedded: Mix.env == :prod,
			start_permanent: Mix.env == :prod,
			aliases: aliases,
			deps: deps
		]
	end

	def application do
		[
			mod: { Whitefox, [] },
			applications: [
				:phoenix,
				:cowboy,
				:logger,
				:phoenix_ecto,
				:postgrex,
				:timex,
				:comeonin
			]
		]
	end

	defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
	defp elixirc_paths(_),     do: ["lib", "web"]

	defp deps do
		[
			# Framework
			{:phoenix, "~> 1.1.4"},
			{:postgrex, ">= 0.0.0"},
			{:phoenix_ecto, "~> 2.0"},
			{:phoenix_html, "~> 2.5.1"},
			{:cowboy, "~> 1.0"},

			# Application Specific
			{:cors_plug, "~> 0.1.4"},
			{:timex, "~> 1.0.1"},
			{:timex_ecto, "~> 0.8.0"},
			{:scrivener, "~> 1.0"},
			{:secure_random, "~> 0.2"},
			{:comeonin, "~> 2.1"},
			{:mailgun, "~> 0.1.2"}
		]
	end

	defp aliases do
		[
			"ecto.setup": [
				"ecto.create",
				"ecto.migrate",
				"run priv/repo/seeds.exs"
			],
			"ecto.reset": [
				"ecto.drop",
				"ecto.setup"
			]
		]
	end
end
