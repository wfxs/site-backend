import Comeonin.Pbkdf2

Whitefox.Repo.insert!(%Whitefox.User{
	handle: "avitex",
	name: "James",
	email: "theavitex@gmail.com",
	permission_level: 3,
	encrypted_password: hashpwsalt("Carbonfibre1010")
})

Whitefox.Repo.insert!(%Whitefox.Articles.Article{
	title: "Welcome to the Beta",
	body: "This site is currently under development",
	author_id: 1,
})

Whitefox.Repo.insert!(%Whitefox.Forum.Category{
	title: "Whitefox eSports",
	description: "The forums for the Whitefox eSports Community",
	author_id: 1,
	parent_id: nil
})

Whitefox.Repo.insert!(%Whitefox.Forum.Category{
	title: "Gaming",
	description: "The very best",
	author_id: 1,
	parent_id: 1
})

Whitefox.Repo.insert!(%Whitefox.Forum.Thread{
	title: "DayZ",
	author_id: 1,
	category_id: 2
})

Whitefox.Repo.insert!(%Whitefox.Forum.Post{
	body: "Hell yeah",
	author_id: 1,
	thread_id: 1
})
