defmodule Whitefox.Repo.Migrations.CreateTokens do
	use Ecto.Migration

	def change do
		create table(:user_tokens, primary_key: false) do
			add :hash,       :string, primary_key: true
			add :refresh,    :string
			add :lifetime,   :integer
			add :last_used,  :datetime
			add :user_id,    references(:users, on_delete: :nothing)

			timestamps
		end

		create index(:user_tokens, [:hash])
		create index(:user_tokens, [:user_id])
		create index(:user_tokens, [:refresh], unique: true)
	end
end
