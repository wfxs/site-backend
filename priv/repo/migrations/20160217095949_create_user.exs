defmodule Whitefox.Repo.Migrations.CreateUser do
	use Ecto.Migration

	def change do
		create table(:users) do
			add :handle,             :string
			add :name,               :string
			add :date_of_birth,      :datetime
			add :location,           :string
			add :encrypted_password, :string

			# NOTE: Will implement better
			# systems later
			add :email,              :string
			add :verification_code,  :string
			add :permission_level,   :integer

			timestamps
		end
		create index(:users, [:handle], unique: true)
		create index(:users, [:email],  unique: true)

	end
end
