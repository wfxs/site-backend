defmodule Whitefox.Repo.Migrations.CreateForum do
	use Ecto.Migration

	def change do
		create table(:categories) do
			add :title,       :string
			add :description, :string
			add :author_id,   references(:users,      on_delete: :nothing)
			add :parent_id,   references(:categories, on_delete: :nothing)

			timestamps
		end
		create index(:categories, [:author_id])
		create index(:categories, [:parent_id])

		create table(:threads) do
			add :title, :string
			add :author_id,   references(:users,      on_delete: :nothing)
			add :category_id, references(:categories, on_delete: :nothing)

			timestamps
		end
		create index(:threads, [:author_id])
		create index(:threads, [:category_id])

		create table(:posts) do
			add :body,      :string
			add :author_id, references(:users,   on_delete: :nothing)
			add :thread_id, references(:threads, on_delete: :nothing)

			timestamps
		end
		create index(:posts, [:author_id])
		create index(:posts, [:thread_id])
	end
end
