ExUnit.start

Mix.Task.run "ecto.reset", ~w(-r Whitefox.Repo --quiet)

Whitefox.Repo.insert!(%Whitefox.UserToken{
	hash: "test_token",
	refresh: "test_token_refresh",
	user_id: 1
})

Ecto.Adapters.SQL.begin_test_transaction(Whitefox.Repo)
