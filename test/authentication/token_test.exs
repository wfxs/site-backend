defmodule Whitefox.AuthenticationTest do
	alias Whitefox.Token

	use Whitefox.ConnCase

	@user %{ id: 1 }

	test "token assignment for user" do
		case Token.assign(@user) do
			{:ok, token} ->
				assert token
			{:error, error} ->
				flunk("Token creation failed: " <> error)
		end
	end

	test "token extraction from header", %{conn: conn} do
		conn = put_req_header(conn, "authorization", "Token 123")

		assert Token.extract_hash(conn) == "123"
	end

	test "token retrieval for user", %{ conn: conn } do
		{ :ok, created_token } = Token.assign(@user)

		conn = put_req_header(conn, "authorization", "Token " <> created_token.hash)

		case Token.retrieve(conn) do
			{:ok, token} ->
				assert token.hash == created_token.hash

				if !is_nil(token.last_used) && !is_nil(created_token.last_used) do
					assert token.last_used != created_token.last_used
				end

			{:error, error} ->
				flunk("Token retrieval failed: " <> error)
		end
	end

	test "token refresh" do
		{ :ok, created_token } = Token.assign(@user)

		case Token.refresh(created_token.refresh) do
			{:ok, token} ->
				assert token.hash != created_token.hash
			{:error, error} ->
				flunk("Token refresh failed: " <> error)
		end
	end
end
