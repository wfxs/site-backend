defmodule Whitefox.Forum.PostTest do
	use Whitefox.ModelCase

	alias Whitefox.Forum.Post

	@valid_attrs %{body: "some content", author_id: 1, thread_id: 1}
	@invalid_attrs %{}

	test "changeset with valid attributes" do
		changeset = Post.changeset(%Post{}, @valid_attrs)
		assert changeset.valid?
	end

	test "changeset with invalid attributes" do
		changeset = Post.changeset(%Post{}, @invalid_attrs)
		refute changeset.valid?
	end
end
