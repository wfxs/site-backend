defmodule Whitefox.Forum.CategoryTest do
	use Whitefox.ModelCase

	alias Whitefox.Forum.Category

	@valid_attrs %{description: "some content", title: "some content", parent_id: 1, author_id: 1}
	@invalid_attrs %{}

	test "changeset with valid attributes" do
		changeset = Category.changeset(%Category{}, @valid_attrs)
		assert changeset.valid?
	end

	test "changeset with invalid attributes" do
		changeset = Category.changeset(%Category{}, @invalid_attrs)
		refute changeset.valid?
	end
end
