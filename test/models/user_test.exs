defmodule Whitefox.UserTest do
	use Whitefox.ModelCase

	alias Whitefox.User

	@valid_attrs %{date_of_birth: "2010-04-17 14:00:00", email: "bob@gmail.com", handle: "bob", location: "some content", name: "some content", password: "some content", password_confirmation: "some content"}
	@invalid_attrs %{}

	test "changeset with valid attributes" do
		changeset = User.changeset(%User{}, @valid_attrs)
		assert changeset.valid?
	end

	test "changeset with invalid attributes" do
		changeset = User.changeset(%User{}, @invalid_attrs)
		refute changeset.valid?
	end
end
