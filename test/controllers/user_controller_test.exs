defmodule Whitefox.UserControllerTest do
	use Whitefox.ConnCase

	alias Whitefox.User
	@valid_attrs %{date_of_birth: "2010-04-17 14:00:00", email: "bob@gmail.com", handle: "bob", location: "some content", name: "some content", password: "some content", password_confirmation: "some content"}
	@invalid_attrs %{}

	setup %{conn: conn} do
		conn = conn
			|> put_req_header("authorization", "Token test_token")
			|> put_req_header("accept", "application/json")

		{:ok, conn: conn }
	end

	test "lists all entries on index", %{conn: conn} do
		conn = get conn, user_path(conn, :index)
		assert json_response(conn, 200)["users"]
	end

	test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
		assert_error_sent 404, fn ->
			get conn, user_path(conn, :show, -1)
		end
	end

	test "creates and renders resource when data is valid", %{conn: conn} do
		conn = post conn, user_path(conn, :create), @valid_attrs
		assert json_response(conn, 201)["id"]
		assert Repo.get_by(User, handle: @valid_attrs[:handle])
	end

	test "does not create resource and renders errors when data is invalid", %{conn: conn} do
		conn = post conn, user_path(conn, :create), @invalid_attrs
		assert json_response(conn, 422)["error"] != %{}
	end

	test "updates and renders chosen resource when data is valid", %{conn: conn} do
		user = Repo.insert! %User{}
		conn = put conn, user_path(conn, :update, user), @valid_attrs
		assert json_response(conn, 200)["id"]
		assert Repo.get_by(User, handle: @valid_attrs[:handle])
	end

	test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
		user = Repo.insert! %User{}
		conn = put conn, user_path(conn, :update, user), @invalid_attrs
		assert json_response(conn, 422)["error"] != %{}
	end

	test "deletes chosen resource", %{conn: conn} do
		user = Repo.insert! %User{}
		conn = delete conn, user_path(conn, :delete, user)
		assert response(conn, 204)
		refute Repo.get(User, user.id)
	end
end
