defmodule Whitefox.TokenControllerTest do
	use Whitefox.ConnCase

	alias Whitefox.Token
	alias Whitefox.UserToken

	@user %{ id: 1 }
	@credentials_request %{ "type" => "credentials", "login" => "avitex", "password" => "Carbonfibre1010"}

	setup %{conn: conn} do
		{:ok, conn: put_req_header(conn, "accept", "application/json")}
	end

	test "create token from credentials", %{conn: conn} do
		conn = post conn, token_path(conn, :create), @credentials_request
		assert json_response(conn, 201)["hash"]
	end

	test "fail create token from credentials", %{conn: conn} do
		conn = post conn, token_path(conn, :create), Map.put(@credentials_request, "login", "lol")
		assert json_response(conn, 401)["error"]
	end

	test "create token from refresh token", %{conn: conn} do
		{:ok, token} = Token.assign(@user)
		request = %{ "type" => "refresh", "refresh_token" => token.refresh}
		conn = post conn, token_path(conn, :create), request
		assert json_response(conn, 201)["hash"]
	end

	test "fail create token from refresh token", %{conn: conn} do
		request = %{ "type" => "refresh", "refresh_token" => "adsadsad"}
		conn = post conn, token_path(conn, :create), request
		assert json_response(conn, 401)["error"]
	end

	test "delete token (logout)", %{conn: conn} do
		{:ok, token} = Token.assign(@user)
		conn = delete conn, token_path(conn, :delete, token.hash)
		assert response(conn, 204)
		refute Repo.get(UserToken, token.hash)
	end
end
