defmodule Whitefox.Forum.PostControllerTest do
	use Whitefox.ConnCase

	alias Whitefox.Forum.Post
	@valid_attrs %{body: "some content", author_id: 1, thread_id: 1}
	@invalid_attrs %{author_id: -1}

	setup %{conn: conn} do
		conn = conn
			|> put_req_header("authorization", "Token test_token")
			|> put_req_header("accept", "application/json")

		{:ok, conn: conn }
	end

	test "lists all entries on index", %{conn: conn} do
		conn = get conn, post_path(conn, :index)
		assert json_response(conn, 200)["posts"]
	end

	test "shows chosen resource", %{conn: conn} do
		post = Repo.get!(Post, 1)
		conn = get conn, post_path(conn, :show, post)
		assert json_response(conn, 200)["id"]
	end

	test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
		assert_error_sent 404, fn ->
			get conn, post_path(conn, :show, -1)
		end
	end

	test "creates and renders resource when data is valid", %{conn: conn} do
		conn = post conn, post_path(conn, :create), @valid_attrs
		assert json_response(conn, 201)["id"]
		assert Repo.get_by(Post, @valid_attrs)
	end

	test "does not create resource and renders errors when data is invalid", %{conn: conn} do
		conn = post conn, post_path(conn, :create), @invalid_attrs
		assert json_response(conn, 403)["error"] != %{}
	end

	test "updates and renders chosen resource when data is valid", %{conn: conn} do
		post = Repo.get! Post, 1
		conn = put conn, post_path(conn, :update, post), @valid_attrs
		assert json_response(conn, 200)["id"]
		assert Repo.get_by(Post, @valid_attrs)
	end

	test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
		post = Repo.get! Post, 1
		conn = put conn, post_path(conn, :update, post), @invalid_attrs
		assert json_response(conn, 422)["error"] != %{}
	end

	test "deletes chosen resource", %{conn: conn} do
		post = Repo.insert! %Post{}
		conn = delete conn, post_path(conn, :delete, post)
		assert response(conn, 204)
		refute Repo.get(Post, post.id)
	end
end
