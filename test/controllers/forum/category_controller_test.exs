defmodule Whitefox.Forum.CategoryControllerTest do
	use Whitefox.ConnCase

	alias Whitefox.Forum.Category
	@valid_attrs %{description: "some content", title: "some content", "parent_id": 1, "author_id": 1}
	@invalid_attrs %{}

	setup %{conn: conn} do
		conn = conn
			|> put_req_header("authorization", "Token test_token")
			|> put_req_header("accept", "application/json")

		{:ok, conn: conn }
	end

	test "lists all entries on index", %{conn: conn} do
		conn = get conn, category_path(conn, :index)
		assert json_response(conn, 200)["categories"]
	end

	test "shows chosen resource", %{conn: conn} do
		category = Repo.insert! %Category{}
		conn = get conn, category_path(conn, :show, category)
		assert json_response(conn, 200)["id"]
	end

	test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
		assert_error_sent 404, fn ->
			get conn, category_path(conn, :show, -1)
		end
	end

	test "creates and renders resource when data is valid", %{conn: conn} do
		conn = post conn, category_path(conn, :create), @valid_attrs
		assert json_response(conn, 201)["id"]
		assert Repo.get_by(Category, @valid_attrs)
	end

	test "does not create resource and renders errors when data is invalid", %{conn: conn} do
		conn = post conn, category_path(conn, :create), @invalid_attrs
		assert json_response(conn, 422)["error"] != %{}
	end

	test "updates and renders chosen resource when data is valid", %{conn: conn} do
		category = Repo.insert! %Category{}
		conn = put conn, category_path(conn, :update, category), @valid_attrs
		assert json_response(conn, 200)["id"]
		assert Repo.get_by(Category, @valid_attrs)
	end

	test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
		category = Repo.insert! %Category{}
		conn = put conn, category_path(conn, :update, category), @invalid_attrs
		assert json_response(conn, 422)["error"] != %{}
	end

	test "deletes chosen resource", %{conn: conn} do
		category = Repo.insert! %Category{}
		conn = delete conn, category_path(conn, :delete, category)
		assert response(conn, 204)
		refute Repo.get(Category, category.id)
	end
end
